<?php
	include("connection.php");
	
	
	$query = mysqli_query($conn, "SELECT * FROM user");
	$rowcount = mysqli_num_rows($query);

?>

<html>
<head>
	<style type="text/css">
		* {box-sizing: border-box;}

body { 
  margin: 0;
  font-family: serif;
  background-color: #d6f5f5;
}

.header {
  overflow: hidden;
  background-color: #006666;
  padding: 20px 10px;
}

.header a {
  float: left;
  color: white;
  text-align: center;
  padding: 12px;
  text-decoration: none;
  font-size: 22px; 
  line-height: 25px;
  border-radius: 4px;
}


.header a:hover {
  background-color: #ddd;
  color: black;
}

.header a:active {
  background-color: dodgerblue;
  color: white;
}

.header-right {
  float: right;
}

@media screen and (max-width: 500px) {
  .header a {
    float: none;
    display: block;
    text-align: left;
  }
  .header-right {
    float: none;
  }
}
		#stddiv
		{
			border-style: double;
			border-bottom: 0px;
			width:800px; 
			margin-left: 282px;
			float: left;
			margin-top: 50px;
		}
		button
		{
			height: 40px;
			width: 120px;
			float: right;
			background-color: dodgerblue;
			border: 0px;
			font-weight: bold; 
			font-family: serif;
			font-size: 20px;
			margin-top: -60px;
			margin-right: 50px;
		}
	</style>
</head>
<body>
	<div class="header">
  	<div class="header-right">
	    <a href="select.php">Home</a>
	    <a href="insert.php">Student Form</a>
  	</div>
	</div>

	<div id="msgdiv" style="height: 20px; width:300px;color: red;font-weight: bold;size: 50px;"></div>

	 <div id="stddiv">
	 	<h1 style="margin-left: 100px;">Student Record</h1>
		<button name="redirect" onclick="redirect()">Add</button>
	</div>

    <script type="text/javascript">
    function redirect()
    { 
    window.location= "http://localhost:3000/php/insert.php";
    }
    </script>


<table style="clear: both;" border="1px" align="center" width="800px">
	<tr>
		<th>Id</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Father Name</th>
		<th>Roll No</th>
		<th>Gender</th>
		<th>Status</th>
		<th>Subject</th>
		<th>Image</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr>
<?php
	for($i=1 ; $i<=$rowcount ; $i++)
	{
		$row = mysqli_fetch_array($query);
?>
		<tr>
		<td><?php echo $row["id"]?></td>
		<td><?php echo $row["first_name"]?></td>
		<td><?php echo $row["last_name"]?></td>
		<td><?php echo $row["father_name"]?></td>
		<td><?php echo $row["roll_no"]?></td>
		<td><?php echo $row["gender"]?></td>
		<td><?php echo $row["status"]?></td>
		<td><?php echo $row["subject"]?></td>
		<td><img src="image/<?php echo $row['file'] ?>" height="100" width="100" alt="Image"></td> 
		<td><a href="edit.php?id=<?php echo $row['id']; ?>" class="edit" onClick="">Edit</a></td>
 		<td><a href="del.php?id=<?php echo $row['id']; ?>"  onClick="delmsg()" class="delete">Delete</a></td>
	</tr>
<?php
	}
?>
</table>
</body>
</html>

<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>

<script>
function delmsg() {
     alert("Are you sure you want to delete?");
     	document.getElementById("msgdiv").innerHTML="Record is deleted successfully.";
    
}
</script>

<!-- jquery for edit confirmation -->

<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
    $("a.edit").click(function(e){
        if(!confirm('Are you sure you want to edit?')){
            e.preventDefault();
            return false;
        }

        return true;
    });
});
</script>

	<!-- jquery for delete confirmation -->

<!-- <script language="JavaScript" type="text/javascript">
$(document).ready(function(){
    $("a.delete").click(function(e){
        if(!confirm('Are you sure you want to delete?')){
            e.preventDefault();
            return false;
             
        }
       $("#msgdiv").fadeIn();
        return true;
    });
});
</script> -->